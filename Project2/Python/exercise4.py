

from simulation_parameters import SimulationParameters
from util.run_open_loop import run_single
import numpy as np
import farms_pylog as pylog
import os
import matplotlib.pyplot as plt
from plot_results import plot_exercise_multiple


def exercise4():

    pylog.info("Ex 4")
    pylog.info("Implement exercise 4")
    log_path = './logs/exercise4/'
    os.makedirs(log_path, exist_ok=True)
    """
    nsim = 3 # as we change I from 0 to 30, so one simulation per entire value

    pylog.info(
        "Running multiple simulations in parallel from a list of SimulationParameters")
    pars_list = [
        SimulationParameters(
            simulation_i=i,
            n_iterations=3001,
            log_path=log_path,
            video_record=False,
            compute_metrics=2,
            I=I,
            headless=True,
            print_metrics=True
        )
        for i, I in enumerate(np.linspace(0, 30, nsim))
    ]"""
    #amplitudes = np.zeros(26)
    #frequencies = np.zeros(26)
    for i in range(1,27):
        all_pars = SimulationParameters(
            simulation_i=i,
            n_iterations=3001, #5001
            log_path=log_path,
            compute_metrics=2, #only need mechanical metrics
            return_network=True,
            headless=True,
            print_metrics=True,
            I = i,
        )
        pylog.info("Running simulation")
        print(all_pars.simulation_i)
        controller = run_single(
            all_pars
        )
        #amplitudes[i-1] = controller.pars.amp
        #frequencies[i-1] = controller.pars.frequency


    """
    run_multiple(pars_list, num_process=8)
    print(log_path+"controller")
    pylog.info("Running the simulation")
    controller = run_single(
        all_pars
    )
    #plot_exercise_multiple(nsim, log_path)
    """


if __name__ == '__main__':
    exercise4()
    #plt.show()
