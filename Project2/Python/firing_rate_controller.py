"""Network controller"""

import numpy as np
from scipy.interpolate import CubicSpline
import scipy.stats as ss
import farms_pylog as pylog


class FiringRateController:
    """zebrafish controller"""

    def __init__(
            self,
            pars
    ):
        super().__init__()

        self.n_iterations = pars.n_iterations
        self.n_neurons = pars.n_neurons
        self.n_muscle_cells = pars.n_muscle_cells
        self.timestep = pars.timestep
        self.times = np.linspace(
            0,
            self.n_iterations *
            self.timestep,
            self.n_iterations)
        self.pars = pars

        self.n_eq = self.n_neurons*4 + self.n_muscle_cells*2 + self.n_neurons * \
            2  # number of equations: number of CPG eq+muscle cells eq+sensors eq
        self.muscle_l = 4*self.n_neurons + 2 * \
            np.arange(0, self.n_muscle_cells)  # muscle cells left indexes
        self.muscle_r = self.muscle_l+1  # muscle cells right indexes
        self.all_muscles = 4*self.n_neurons + \
            np.arange(0, 2*self.n_muscle_cells)  # all muscle cells indexes
        # vector of indexes for the CPG activity variables - modify this
        # 
        self.rate_l = 4 *\
            np.arange(0, self.n_neurons)  # firing rate of the left CPG neuron indexes
        self.adaptation_l = self.rate_l +1 # firing rate adaptation left indexes
        self.rate_r = self.rate_l + 2 #  firing rate of the right CPG neuron indexes
        self.adaptation_r = self.rate_l +3 # firing rate adaptation right indexes


        self.all_v = range(self.n_neurons*2)


        self.state = np.zeros([self.n_iterations, self.n_eq])  # equation state
        self.dstate = np.zeros([self.n_eq])  # derivative state
        self.state[0] = np.random.rand(self.n_eq)  # set random initial state

        self.poses = np.array([
            0.007000000216066837,
            0.00800000037997961,
            0.008999999612569809,
            0.009999999776482582,
            0.010999999940395355,
            0.012000000104308128,
            0.013000000268220901,
            0.014000000432133675,
            0.014999999664723873,
            0.01600000075995922,
        ])  # active joint distances along the body (pos=0 is the tip of the head)
        self.poses_ext = np.linspace(
            self.poses[0], self.poses[-1], self.n_neurons)  # position of the sensors

        # initialize ode solver
        self.f = self.ode_rhs

        # stepper function selection
        if self.pars.method == "euler":
            self.step = self.step_euler
        elif self.pars.method == "noise":
            self.step = self.step_euler_maruyama
            # vector of noise for the CPG voltage equations (2*n_neurons)
            self.noise_vec = np.zeros(self.n_neurons*2)

        # zero vector activations to make first and last joints passive
        # pre-computed zero activity for the first 4 joints
        self.zeros8 = np.zeros(8)
        # pre-computed zero activity for the tail joint
        self.zeros2 = np.zeros(2)

    def get_ou_noise_process_dw(self, timestep, x_prev, sigma):
        """
        Implement here the integration of the Ornstein-Uhlenbeck processes
        dx_t = -0.5*x_t*dt+sigma*dW_t
        Parameters
        ----------
        timestep: <float>
            Timestep
        x_prev: <np.array>
            Previous time step OU process
        sigma: <float>
            noise level
        Returns
        -------
        x_t{n+1}: <np.array>
            The solution x_t{n+1} of the Euler Maruyama scheme
            x_new = x_prev-0.1*x_prev*dt+sigma*sqrt(dt)*Wiener
        """

        dx_process = np.zeros_like(x_prev)

    def step_euler(self, iteration, time, timestep, pos=None):
        """Euler step"""
        self.state[iteration+1, :] = self.state[iteration, :] + \
            timestep*self.f(time, self.state[iteration], pos=pos)
        return np.concatenate([
            self.zeros8,  # the first 4 passive joints
            self.motor_output(iteration),  # the active joints
            self.zeros2  # the last (tail) passive joint
        ])

    def step_euler_maruyama(self, iteration, time, timestep, pos=None):
        """Euler Maruyama step"""
        self.state[iteration+1, :] = self.state[iteration, :] + \
            timestep*self.f(time, self.state[iteration], pos=pos)
        self.noise_vec = self.get_ou_noise_process_dw(
            timestep, self.noise_vec, self.pars.noise_sigma)
        self.state[iteration+1, self.all_v] += self.noise_vec
        self.state[iteration+1,self.all_muscles] = np.maximum(self.state[iteration+1,self.all_muscles],0)  # prevent from negative muscle activations
        return np.concatenate([
            self.zeros8,  # the first 4 passive joints
            self.motor_output(iteration),  # the active joints
            self.zeros2  # the last (tail) passive joint
        ])

    def motor_output(self, iteration):
        """
        Here you have to final muscle activations for the 10 active joints.
        It should return an array of 2*n_muscle_cells=20 elements,
        even indexes (0,2,4,...) = left muscle activations
        odd indexes (1,3,5,...) = right muscle activations
        """
        self.state[iteration,self.all_muscles] = self.pars.w_V2a2muscle  * self.state[iteration,self.all_muscles]
        return self.state[iteration,self.all_muscles]
        return np.zeros(
            2 *
            self.n_muscle_cells)  # here you have to final active muscle equations for the 10 joints

    def ode_rhs(self,  _time, state, pos=None):
        """Network_ODE
        You should implement here the right hand side of the system of equations
        Parameters
        ----------
        _time: <float>
            Time
        state: <np.array>
            ODE states at time _time
        Returns
        -------
        dstate: <np.array>
            Returns derivative of state
        """

        """Construct matrix Win"""
        Win = np.zeros((self.n_neurons,self.n_neurons))
        for i in range(self.n_neurons):
            for j in range(self.n_neurons):
                if(i<=j and (j-i)<=self.pars.n_asc):
                    Win[i][j] = 1/(j-i+1)
                elif(i>j and (i-j)<=self.pars.n_desc):
                    Win[i][j] = 1/(i-j+1)
                else:
                    Win[i][j] = 0

        Wmc = np.zeros((self.n_muscle_cells,self.n_neurons))
        for i in range(self.n_muscle_cells):
            for j in range(self.n_neurons):
                if(self.pars.n_mc*i<=j and j<=(self.pars.n_mc*(i+1)-1)):
                    Wmc[i][j]=1
                else :
                    Wmc[i][j]=0


        """Question 3 : equations for the firing rate controller"""
        self.dstate[self.rate_l]   = (
            -state[self.rate_l] + np.sqrt(
                np.maximum(
                    (self.pars.I - self.pars.b*state[self.adaptation_l] - (self.pars.g_in*Win @ state[self.rate_r])),np.zeros(50)
                    )
                )
            )/self.pars.tau  
        
        self.dstate[self.adaptation_l] = (-state[self.adaptation_l] + self.pars.rho*state[self.rate_l])/self.pars.taua
        
        #print(self.dstate)

        self.dstate[self.rate_r] = (
            -state[self.rate_r] + np.sqrt(
                np.maximum(
                    (self.pars.I - self.pars.b*state[self.adaptation_r] - (self.pars.g_in*Win @ state[self.rate_l])),np.zeros(50)
                    )
                )
            )/self.pars.tau
        self.dstate[self.adaptation_r] = (-state[self.adaptation_r] + self.pars.rho*state[self.rate_r])/self.pars.taua


        self.dstate[self.muscle_l] =(self.pars.g_mc*Wmc @ state[self.rate_l]*(1-state[self.muscle_l]))/self.pars.taum_a - state[self.muscle_l]/self.pars.taum_d
        self.dstate[self.muscle_r] =(self.pars.g_mc*Wmc @ state[self.rate_r]*(1-state[self.muscle_r]))/self.pars.taum_a - state[self.muscle_r]/self.pars.taum_d

        return self.dstate

