"""Network controller"""

import numpy as np
import farms_pylog as pylog

from metrics import compute_all
from simulation_parameters import SimulationParameters


class WaveController:

    """Test controller"""

    def __init__(self, pars):
        self.pars = pars
        self.timestep = pars.timestep
        self.times = np.linspace(
            0,
            pars.n_iterations *
            pars.timestep,
            pars.n_iterations)
        self.controller = pars.controller
        self.n_joints = pars.n_joints

        # state array for recording all the variables
        self.state = np.zeros((pars.n_iterations, 2*self.n_joints))

        #pylog.warning(
        #    "Implement below the step function following the instructions here and in the report")

        # indexes of the left muscle activations (optional)
        self.muscle_l = 2*np.arange(15)
        # indexes of the right muscle activations (optional)
        self.muscle_r = self.muscle_l+1

    def step(self, iteration, time, timestep, pos=None):
        """
        Step function. This function passes the activation functions of the muscle model
        Inputs:
        - iteration - iteration index
        - time - time vector
        - timestep - integration timestep
        - pos (not used) - joint angle positions

        Implement here the control step function,
        it should return an array of 2*n_joint=30 elements,
        even indexes (0,2,4,...) = left muscle activations
        odd indexes (1,3,5,...) = right muscle activations

        In addition to returning the activation functions, store
        them in self.state for later use offline
        """


        time = timestep*iteration;

        #print(self.pars.amp);
        
        # si probleme, mettre iteration + 1
        #for i in self.muscle_l:
        #    self.state[iteration,i] = 0.5 + self.amp/2 * np.sin(2*np.pi*(self.frequency*time - self.epsilon*i/self.n_joints))
        #for i in self.muscle_r:
        #    self.state[iteration,i] = 0.5 - self.amp/2 * np.sin(2*np.pi*(self.frequency*time - self.epsilon*i/self.n_joints))
        
        """EXERCICE 1 AND 2"""
        if self.pars.controller == "sine":
            self.state[iteration+1,self.muscle_l] = 0.5 + self.pars.amp *0.5 * np.sin(2*np.pi*(self.pars.frequency*time - self.pars.epsilon*self.muscle_l/self.n_joints))
            self.state[iteration+1,self.muscle_r] = 0.5 - self.pars.amp/2 * np.sin(2*np.pi*(self.pars.frequency*time - self.pars.epsilon*self.muscle_r/self.n_joints))
        
    
        """EXERCISE 3"""
        if self.pars.controller == "square" :
            self.state[iteration+1,self.muscle_l] =  0.5+ self.pars.amp *0.5*2*(-0.5+1 / (1+np.exp(-self.pars.lam*(np.sin(2*np.pi*(self.pars.frequency*time - self.pars.epsilon*self.muscle_l/self.n_joints))))))
            self.state[iteration+1,self.muscle_r] =  0.5- self.pars.amp *0.5*2*(-0.5+1 / (1+np.exp(-self.pars.lam*(np.sin(2*np.pi*(self.pars.frequency*time - self.pars.epsilon*self.muscle_r/self.n_joints))))))


        return self.state[iteration,:] 
        # state is a matrice [iteration, n of muscle]




