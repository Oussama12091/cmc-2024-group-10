
from util.run_closed_loop import run_multiple
from util.run_closed_loop import pretty
from simulation_parameters import SimulationParameters
from plot_results import plot_cot_multiple, plot_exercise_multiple
import os
import numpy as np
import farms_pylog as pylog


def exercise1():

    pylog.info("Ex 1")
    log_path = './logs/exercise1/'
    os.makedirs(log_path, exist_ok=True)
    print("Log path: ", log_path)

    nsim = 5

    pars_list = [
        SimulationParameters(
            simulation_i=i*nsim+j,
            n_iterations=3001,
            log_path=log_path,
            video_record=False,
            compute_metrics=3,
            epsilon=epsilon,
            controller="sine",
            amp=amp,
            headless=True,  # need to stay True or the simulation will crash
            print_metrics=False # true for printing the metrics at the end, false otherwise
        )
        for j, amp in enumerate(np.linspace(0.05, 1.9, nsim))
        for i, epsilon in enumerate(np.linspace(0.05, 1.9, nsim))
    ]

    pylog.info(
        "Running multiple simulations in parallel from a list of SimulationParameters")
    run_multiple(pars_list, num_process=16)
    plot_exercise_multiple(nsim*nsim, log_path)



if __name__ == '__main__':
    exercise1()

